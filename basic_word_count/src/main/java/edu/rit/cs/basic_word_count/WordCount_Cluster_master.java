package edu.rit.cs.basic_word_count;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @file WordCount_Cluster_master.java
 * @author Ryan Missel
 *
 * Class that represents the master node for a distributed wordcount program, sending out the paritioned data
 * and then merging it all together once the workers reach back
 */
public class WordCount_Cluster_master {
    // Final wordcount array that holds the sorted lists
    private static Map<String, Integer> wordcount = new TreeMap<>();

    // Absolute path to the Reviews.csv, needed for Maven pathing
    public static final String AMAZON_FINE_FOOD_REVIEWS_file=System.getProperty("user.dir") +
            "/amazon-fine-food-reviews/Reviews.csv";


    public static List<AmazonFineFoodReview> read_reviews(String dataset_file) {
        List<AmazonFineFoodReview> allReviews = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(dataset_file))){
            String reviewLine = null;
            // read the header line
            reviewLine = br.readLine();

            //read the subsequent lines
            while ((reviewLine = br.readLine()) != null) {
                allReviews.add(new AmazonFineFoodReview(reviewLine));
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return allReviews;
    }

    /**
     * Handles printing the counted word list
     * @param wordcount map of the strings and number count
     */
    public static void print_word_count( Map<String, Integer> wordcount){
        for(String word : wordcount.keySet()){
            System.out.println(word + " : " + wordcount.get(word));
        }
    }

    /**
     * Main thread of the server that handles getting the split indices for the partitions, accepting worker node
     * requests, and creating individual threads for each worker node. Merges all the returned maps afterwards
     * @param args command line args
     */
    public static void main(String[] args) {
        List<AmazonFineFoodReview> allReviews = read_reviews(AMAZON_FINE_FOOD_REVIEWS_file);

        // Starting the timers
        MyTimer myTimer = new MyTimer("wordCount");
        myTimer.start_timer();

        // Partitioning the original dataset
        ArrayList<ArrayList<AmazonFineFoodReview>> paritions = new ArrayList<>();
        int counter = 0;
        Integer splits = allReviews.size() / 4;
        for(int i = 0; i < 4; i++){
            int start = splits * i;
            int end = start + splits;
            paritions.add(new ArrayList<>(allReviews.subList(start, end)));
        }

        // Storing active threads to join on
        ArrayList<Connection> connections = new ArrayList<>();

        // Loop to receive connections up to 4 total worker nodes, breaks loop once 4 are reached
        try{
            int serverPort = 7896;
            ServerSocket listenSocket = new ServerSocket(serverPort);
            System.out.println("Server up!");
            while (counter < 4) {
                Socket clientSocket = listenSocket.accept();
                Connection c = new Connection(clientSocket, paritions.get(counter));
                connections.add(c);
                counter += 1;
            }
        } catch(IOException e){
            System.out.println("Listen :" + e.getMessage());
        }

        // Joining on any still active threads
        for (Connection con: connections){
            try {
                con.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        // Merging together after all threads have returned
        for(Connection con: connections){
            for(Map.Entry<String, Integer> e: con.getSorted().entrySet()){
                wordcount.merge(e.getKey(), e.getValue(), Integer::sum);
            }
        }

        // Stopping timers and printing the array
        myTimer.stop_timer();
        print_word_count(wordcount);
        myTimer.print_elapsed_time();
    }

}
