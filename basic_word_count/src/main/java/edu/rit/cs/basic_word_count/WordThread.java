package edu.rit.cs.basic_word_count;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WordThread implements Runnable{
    /**
     Class that represents a single thread and run on a parition of the word count thread.
     Takes in a list of strings as the partition

     List<AmazonFineFoodReview> reviews: list of amazon reviews to parse
     Map<String, Integer> map: end map after counting words
     int id: id of the thread for printing purposes
     */
    private List<AmazonFineFoodReview> reviews;
    private Map<String, Integer> map;
    private int id;

    public WordThread(List<AmazonFineFoodReview> reviews, int id){
        this.reviews = reviews;
        this.id = id;
    }

    /**
     Run function of the WordThread class, tokenizing then counting a provided list
     */
    @Override
    public void run() {
        // Tokenize the list
        List<String> words = new ArrayList<String>();
        for(AmazonFineFoodReview review : reviews) {
            Pattern pattern = Pattern.compile("([a-zA-Z]+)");
            Matcher matcher = pattern.matcher(review.get_Summary());

            while(matcher.find())
                words.add(matcher.group().toLowerCase());
        }

        // Sequential counting of the list, using a HashMap for efficiency
        Map<String, Integer> tempcount = new HashMap<>();
        for(String word : words) {
            if(!tempcount.containsKey(word)) {
                tempcount.put(word, 1);
            } else{
                int init_value = tempcount.get(word);
                tempcount.replace(word, init_value, init_value+1);
            }
        }

        // Merging tempcount into wordcount
        this.map = tempcount;
        System.out.println("Thread " + this.id + " complete!");
    }

    /**
     * Simple getter for the Map holding the counted words
     * @return this.map
     */
    public Map<String, Integer> getSorted(){
        return this.map;
    }
}
