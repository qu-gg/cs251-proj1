package edu.rit.cs.basic_word_count;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;


/**
 * @file WordCount_Threads.java
 * @author Ryan Missel
 *
 * Class that represents a wordcount program that is threaded to enchance efficieny.
 */
public class WordCount_Threads {
    // Final wordcount array that holds the sorted lists
    private static Map<String, Integer> wordcount = new TreeMap<>();
    public static final String AMAZON_FINE_FOOD_REVIEWS_file="basic_word_count/amazon-fine-food-reviews/Reviews.csv";


    public static List<AmazonFineFoodReview> read_reviews(String dataset_file) {
        List<AmazonFineFoodReview> allReviews = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(dataset_file))){
            String reviewLine = null;
            // read the header line
            reviewLine = br.readLine();

            //read the subsequent lines
            while ((reviewLine = br.readLine()) != null) {
                allReviews.add(new AmazonFineFoodReview(reviewLine));
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return allReviews;
    }

    /**
     * Handles printing the counted word list
     * @param wordcount map of the strings and number count
     */
    public static void print_word_count( Map<String, Integer> wordcount){
        for(String word : wordcount.keySet()){
            System.out.println(word + " : " + wordcount.get(word));
        }
    }


    public static void main(String[] args) {
        List<AmazonFineFoodReview> allReviews = read_reviews(AMAZON_FINE_FOOD_REVIEWS_file);

        /* For debug purpose */
//        for(AmazonFineFoodReview review : allReviews){
//            System.out.println(review.get_Text());
//        }

        MyTimer myTimer = new MyTimer("wordCount");
        myTimer.start_timer();

        // Splits the list into 4 partitions to send to the different threads
        List<Thread> threads = new ArrayList<>();
        List<WordThread> word_threads = new ArrayList<>();
        Integer splits = allReviews.size() / 4;
        for(int i = 0; i < 4; i++){
            int start = splits * i;
            int end = start + splits;

            // Start threads
            WordThread wt = new WordThread(allReviews.subList(start, end), i);
            Thread thread = new Thread(wt);
            thread.start();

            // Add thread and word thread to lists
            threads.add(thread);
            word_threads.add(wt);
            System.out.println("Thread " + i + " started!");
        }

        // Joining all the started threads to the main thread
        for (Thread thread: threads){
            try {
                thread.join();
            } catch (InterruptedException e) {
                System.out.println("Exception caught!");
                e.printStackTrace();
            }
        }

        // Merging maps at the end
        for(WordThread wt: word_threads){
            for(Map.Entry<String, Integer> e: wt.getSorted().entrySet()){
                wordcount.merge(e.getKey(), e.getValue(), Integer::sum);
            }
        }

        // Stopping timers and printing the array
        myTimer.stop_timer();
        print_word_count(wordcount);
        myTimer.print_elapsed_time();
    }

}
