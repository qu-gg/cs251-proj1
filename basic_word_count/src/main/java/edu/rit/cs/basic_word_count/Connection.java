package edu.rit.cs.basic_word_count;

import java.io.*;
import java.net.Socket;
import java.util.*;

/**
 * @file Connection.java
 * @author Ryan Missel
 *
 * Class that represents a single connection between the master and worker nodes
 */
public class Connection extends Thread{
    /**
     * ObjectInputStream in: input stream from the worker node
     * ObjectOutputStream out: stream to the worker node
     * Socket clientSocket: socket for the connection between main and this thread
     * ArrayList<AmazonFineFoodReview> partition: piece of data to send to the worker node
     * Map<String, Integer> sorted: sorted map returned from the worker node
     */
    private ObjectInputStream in;
    private ObjectOutputStream out;
    private Socket clientSocket;
    private ArrayList<AmazonFineFoodReview> partition;
    private Map<String, Integer> sorted;

    /**
     * Constructor for a Connection
     * @param aClientSocket socket to a worker node
     * @param partition piece of data to send to the worker
     */
    public Connection(Socket aClientSocket, ArrayList<AmazonFineFoodReview> partition){
        try{
            this.partition = partition;
            this.clientSocket = aClientSocket;
            this.out = new ObjectOutputStream(this.clientSocket.getOutputStream());
            this.in = new ObjectInputStream(this.clientSocket.getInputStream());
            this.start();
        } catch(IOException e){
            System.out.println("Connection: " + e.getMessage());
        }
    }

    /**
     * Run function of the thread that handles passing and receiving data from its corresponding worker node
     */
    public void run(){
        try{
            // Write out partition
            System.out.println("In run!");
            this.out.writeObject(this.partition);

            // Read in sorted list
            this.sorted = (HashMap<String, Integer>) in.readObject();
        } catch (IOException e) {
            System.out.println("IO: " + e.getMessage());
        } catch (ClassNotFoundException e){
            System.out.println("Class: " + e.getMessage());
        } finally {
            try{
                this.clientSocket.close();
            } catch (IOException e){
                /* close failed */
            }
        }
    }

    /**
     * Getter function for the main thread to get the counted parition from this connection
     * @return this.sorted
     */
    public Map<String, Integer> getSorted(){
        return this.sorted;
    }
}

