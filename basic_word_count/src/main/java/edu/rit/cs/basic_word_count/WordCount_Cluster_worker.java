package edu.rit.cs.basic_word_count;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @file WordCount_Cluster_worker.java
 * @author Ryan Missel
 *
 * Class that represents a worker in a distributed wordcount program. Takes in a piece of data, sequentially counts it,
 * and sends it back to the master node for merging.
 */
public class WordCount_Cluster_worker {
    /**
     * Handles printing the counted word list
     * @param wordcount map of the strings and number count
     */
    public static void print_word_count(Map<String, Integer> wordcount){
        for(String word : wordcount.keySet()){
            System.out.println(word + " : " + wordcount.get(word));
        }
    }


    /**
     * Main function of the worker class. Sets up a connection with the server, sends a request for a partition,
     * tokenizes and counts the words, then sends back the counted map
     * @param args
     */
    public static void main(String[] args) {
        System.out.println("Starting...");
        Socket s = null;
        try{
            // Setting up socket connection and sending over request for partition
            int serverPort = 7896;

            while(s == null){
                try{
                    s = new Socket(args[0], serverPort);
                } catch (IOException e) {
                    // Keep trying while socket not connecting
                }
            }

            ObjectOutputStream out = new ObjectOutputStream(s.getOutputStream());
            ObjectInputStream in = new ObjectInputStream(s.getInputStream());
            System.out.println("Sent request...");
            Object data = in.readObject();
            System.out.println("...received partition!");

            // Convert received object to relevant list
            ArrayList<AmazonFineFoodReview> reviews = (ArrayList<AmazonFineFoodReview>) data;

            // Tokenize the list
            ArrayList<String> words = new ArrayList<>();
            for(AmazonFineFoodReview review : reviews) {
                Pattern pattern = Pattern.compile("([a-zA-Z]+)");
                Matcher matcher = pattern.matcher(review.get_Summary());

                while(matcher.find())
                    words.add(matcher.group().toLowerCase());
            }

            // Sequential counting of the list, using a HashMap for efficiency
            Map<String, Integer> tempcount = new HashMap<>();
            for(String word : words) {
                if(!tempcount.containsKey(word)) {
                    tempcount.put(word, 1);
                } else{
                    int init_value = tempcount.get(word);
                    tempcount.replace(word, init_value, init_value+1);
                }
            }

            // Printing out counted partition
            System.out.println("List Sorted!");
            print_word_count(tempcount);

            // Getting output stream and sending over sorted list
            out.writeObject(tempcount);

        } catch(UnknownHostException e) {
            System.out.println("Sock: " + e.getMessage());
        } catch (EOFException e){
            System.out.println("EOF: " + e.getMessage());
        } catch (IOException e) {
            System.out.println("IO: " + e.getMessage());
        } catch (ClassNotFoundException e){
            System.out.println("Class: " + e.getMessage());
        } finally{
            if(s != null){
                try{
                    s.close();
                } catch (IOException e){
                    System.out.println("Close: " + e.getMessage());
                }
            }
        }
    }
}
